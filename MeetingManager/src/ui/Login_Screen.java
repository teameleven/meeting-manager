package ui;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.text.NumberFormat;

import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.text.NumberFormatter;

import manager.Main;

public class Login_Screen extends JFrame
{

	private JPanel contentPane;
	// private JTextField textField;

	/**
	 * Create the frame.
	 */
	public Login_Screen()
	{
		setTitle("Login Screen");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1000, 800);
		GUI.center(this);

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblMenu = new JLabel("Login", SwingConstants.CENTER);
		lblMenu.setFont(new Font("Tahoma", Font.PLAIN, 48));
		lblMenu.setBounds(12, 97, 970, 58);
		contentPane.add(lblMenu);

		JPanel panel = new JPanel();
		panel.setBounds(12, 168, 970, 584);
		contentPane.add(panel);
		panel.setLayout(null);

		JButton btnLogin = new JButton("Login");
		btnLogin.setFont(new Font("Tahoma", Font.PLAIN, 32));
		btnLogin.setBounds(12, 322, 946, 96);
		panel.add(btnLogin);

		NumberFormat format = NumberFormat.getIntegerInstance();
		format.setGroupingUsed(false);
		NumberFormatter formatter = new NumberFormatter(format);
		formatter.setValueClass(Integer.class);
		formatter.setMinimum(-1);
		formatter.setMaximum(99999);
		formatter.setAllowsInvalid(false);
		// If you want the value to be committed on each keystroke instead of focus lost
		formatter.setCommitsOnValidEdit(true);
		JTextField textField = new JFormattedTextField(formatter);

		textField.setFont(new Font("Tahoma", Font.PLAIN, 32));
		textField.setBounds(12, 171, 839, 96);
		panel.add(textField);
		textField.setColumns(10);

		JLabel lblId = new JLabel("ID");
		lblId.setFont(new Font("Tahoma", Font.PLAIN, 32));
		lblId.setBounds(12, 98, 44, 46);
		panel.add(lblId);

		JButton btnDel = new JButton("DEL");
		btnDel.setFont(new Font("Tahoma", Font.PLAIN, 32));
		btnDel.setBounds(863, 171, 95, 96);
		panel.add(btnDel);

		JButton btnBack = new JButton("Back");
		btnBack.setFont(new Font("Tahoma", Font.PLAIN, 24));
		btnBack.setBounds(0, 520, 214, 64);
		panel.add(btnBack);

		textField.addKeyListener(new KeyAdapter()
		{
			@Override
			public void keyPressed(KeyEvent e)
			{
				if (e.getKeyCode() == KeyEvent.VK_ENTER)
				{
					if (textField.getText().length() > 0)
					{
						if (Main.main.login(Integer.parseInt(textField.getText())))
						{
							formatter.setAllowsInvalid(true);
							textField.setText("");
							formatter.setAllowsInvalid(false);
							GUI.showEmployee(Main.main.loggedIn());
						} else
						{
							GUI.showWarning("", "Employee with ID " + textField.getText() + " does not exist");
						}
					} else
					{
						GUI.showWarning("", "Input your ID");
					}
				}
			}
		});

		btnBack.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent e)
			{
				formatter.setAllowsInvalid(true);
				textField.setText("");
				formatter.setAllowsInvalid(false);
				GUI.showMenu();
			}
		});

		btnDel.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent e)
			{
				formatter.setAllowsInvalid(true);
				textField.setText("");
				formatter.setAllowsInvalid(false);
			}
		});

		btnLogin.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				if (textField.getText().length() > 0)
				{
					if (Main.main.login(Integer.parseInt(textField.getText())))
					{
						formatter.setAllowsInvalid(true);
						textField.setText("");
						formatter.setAllowsInvalid(false);
						GUI.showEmployee(Main.main.loggedIn());
					} else
					{
						GUI.showWarning("", "Employee with ID " + textField.getText() + " does not exist");
					}
				} else
				{
					GUI.showWarning("", "Input your ID");
				}
			}
		});

	}

}
