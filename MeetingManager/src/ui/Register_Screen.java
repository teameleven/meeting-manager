package ui;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.text.NumberFormat;

import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.text.NumberFormatter;

import manager.Main;

public class Register_Screen extends JFrame
{

	private JPanel contentPane;
	private JTextField idText;

	/**
	 * Create the frame.
	 */
	public Register_Screen()
	{
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 600, 400);
		GUI.center(this);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton btnLogin = new JButton("Register");
		btnLogin.setFont(new Font("Tahoma", Font.PLAIN, 32));
		btnLogin.setBounds(12, 272, 558, 68);
		contentPane.add(btnLogin);

		NumberFormat format = NumberFormat.getIntegerInstance();
		format.setGroupingUsed(false);
		NumberFormatter formatter = new NumberFormatter(format);
		formatter.setValueClass(Integer.class);
		formatter.setMinimum(-1);
		formatter.setMaximum(99999);
		formatter.setAllowsInvalid(false);
		// If you want the value to be committed on each keystroke instead of focus lost
		formatter.setCommitsOnValidEdit(true);
		JTextField nameText = new JTextField();

		nameText.setFont(new Font("Tahoma", Font.PLAIN, 32));
		nameText.setBounds(135, 158, 276, 81);
		contentPane.add(nameText);
		nameText.setColumns(10);

		JLabel lblId = new JLabel("ID");
		lblId.setFont(new Font("Tahoma", Font.PLAIN, 32));
		lblId.setBounds(23, 69, 44, 46);
		contentPane.add(lblId);

		JButton btnDel = new JButton("DEL");
		btnDel.setFont(new Font("Tahoma", Font.PLAIN, 32));
		btnDel.setBounds(470, 52, 100, 80);
		contentPane.add(btnDel);

		JLabel lblName = new JLabel("Name");
		lblName.setFont(new Font("Tahoma", Font.PLAIN, 32));
		lblName.setBounds(23, 158, 100, 46);
		contentPane.add(lblName);

		idText = new JFormattedTextField(formatter);
		idText.setBounds(138, 52, 276, 81);
		idText.setFont(new Font("Tahoma", Font.PLAIN, 32));
		contentPane.add(idText);
		idText.setColumns(10);

		btnDel.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent e)
			{
				formatter.setAllowsInvalid(true);
				idText.setText("");
				formatter.setAllowsInvalid(false);
			}
		});

		idText.addKeyListener(new KeyAdapter()
		{
			@Override
			public void keyPressed(KeyEvent e)
			{
				if (e.getKeyCode() == KeyEvent.VK_ENTER)
				{
					nameText.requestFocus();
				}
			}
		});

		nameText.addKeyListener(new KeyAdapter()
		{
			@Override
			public void keyPressed(KeyEvent e)
			{
				if (e.getKeyCode() == KeyEvent.VK_ENTER)
				{
					if (nameText.getText().length() < 1 || idText.getText().length() < 5)
					{
						GUI.showWarning("", "Input Correct Details");
					} else
					{
						if (Main.main.getCompany().registerEmployee(nameText.getText(),
						        Integer.parseInt(idText.getText())))
						{
							formatter.setAllowsInvalid(true);
							nameText.setText("");
							formatter.setAllowsInvalid(false);
							dispose();
							GUI.showWarning("", "Register Successful");
						} else
						{
							GUI.showWarning("", "Register Unsuccessful");
						}
					}
				}
			}
		});

		btnLogin.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				if (nameText.getText().length() < 1 || idText.getText().length() < 5)
				{
					GUI.showWarning("", "Input Correct Details");
				} else
				{
					if (Main.main.getCompany().registerEmployee(nameText.getText(), Integer.parseInt(idText.getText())))
					{
						formatter.setAllowsInvalid(true);
						nameText.setText("");
						formatter.setAllowsInvalid(false);
						dispose();
						GUI.showWarning("", "Register Successful");
					} else
					{
						GUI.showWarning("", "Register Unsuccessful");
					}
				}
			}
		});
	}
}
