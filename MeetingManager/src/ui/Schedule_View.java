package ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import manager.Employee;
import manager.Main;
import manager.Week;

public class Schedule_View extends JFrame
{
	private JPanel contentPane;

	private Employee employee;

	private Week week;

	/**
	 * Create the frame.
	 */
	public Schedule_View(Employee employee, String date)
	{
		this.employee = employee;

		setTitle("Schedule View");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, GUI.screenWidth, GUI.screenHeight);
		GUI.center(this);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton btnBack = new JButton("Back");
		btnBack.setFont(new Font("Tahoma", Font.PLAIN, 24));
		btnBack.setBounds(12, 688, 214, 64);
		contentPane.add(btnBack);

		JPanel panel = new JPanel();
		panel.setBounds(26, 103, 929, 540);
		contentPane.add(panel);
		panel.setLayout(null);

		JPanel panel_2 = new JPanel();
		panel_2.setBackground(Color.WHITE);
		panel_2.setBounds(0, 76, 929, 295);
		panel.add(panel_2);
		panel_2.setLayout(new BoxLayout(panel_2, BoxLayout.Y_AXIS));

		JPanel panel_monday = new JPanel();
		panel_monday.setAlignmentX(Component.LEFT_ALIGNMENT);
		panel_monday.setBackground(Color.LIGHT_GRAY);
		panel_monday.setBorder(new EmptyBorder(10, 10, 10, 10));
		panel_2.add(panel_monday);
		panel_monday.setLayout(new BoxLayout(panel_monday, BoxLayout.X_AXIS));

		JLabel lblMonday = new JLabel("Monday");
		lblMonday.setFont(new Font("Tahoma", Font.PLAIN, 32));
		panel_monday.add(lblMonday);

		JPanel panel_tuesday = new JPanel();
		panel_tuesday.setAlignmentX(Component.LEFT_ALIGNMENT);
		panel_tuesday.setBackground(Color.GRAY);
		panel_tuesday.setBorder(new EmptyBorder(10, 10, 10, 10));
		panel_2.add(panel_tuesday);
		panel_tuesday.setLayout(new BoxLayout(panel_tuesday, BoxLayout.X_AXIS));

		JLabel lblTuesday = new JLabel("Tuesday");
		lblTuesday.setFont(new Font("Tahoma", Font.PLAIN, 32));
		panel_tuesday.add(lblTuesday);

		JPanel panel_wednesday = new JPanel();
		panel_wednesday.setAlignmentX(Component.LEFT_ALIGNMENT);
		panel_wednesday.setBackground(Color.LIGHT_GRAY);
		panel_wednesday.setBorder(new EmptyBorder(10, 10, 10, 10));
		panel_2.add(panel_wednesday);
		panel_wednesday.setLayout(new BoxLayout(panel_wednesday, BoxLayout.X_AXIS));

		JLabel lblWednesday = new JLabel("Wednesday");
		lblWednesday.setFont(new Font("Tahoma", Font.PLAIN, 32));
		panel_wednesday.add(lblWednesday);

		JPanel panel_thursday = new JPanel();
		panel_thursday.setAlignmentX(Component.LEFT_ALIGNMENT);
		panel_thursday.setBackground(Color.GRAY);
		panel_thursday.setBorder(new EmptyBorder(10, 10, 10, 10));
		panel_2.add(panel_thursday);
		panel_thursday.setLayout(new BoxLayout(panel_thursday, BoxLayout.X_AXIS));

		JLabel lblThursday = new JLabel("Thursday");
		lblThursday.setFont(new Font("Tahoma", Font.PLAIN, 32));
		panel_thursday.add(lblThursday);

		JPanel panel_friday = new JPanel();
		panel_friday.setAlignmentX(Component.LEFT_ALIGNMENT);
		panel_friday.setBackground(Color.LIGHT_GRAY);
		panel_friday.setBorder(new EmptyBorder(10, 10, 10, 10));
		panel_2.add(panel_friday);
		panel_friday.setLayout(new BoxLayout(panel_friday, BoxLayout.X_AXIS));

		JLabel lblFriday = new JLabel("Friday");
		lblFriday.setFont(new Font("Tahoma", Font.PLAIN, 32));
		panel_friday.add(lblFriday);

		btnBack.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent e)
			{
				GUI.showEmployee(getEmployee());
			}
		});

		try
		{
			week = Main.main.getCompany().getWeek(getEmployee().getID(),
			        Main.main.getCompany().getStartAndEndOfWeek(date));
		} catch (Exception e1)
		{
			e1.printStackTrace();
		}

		if (week == null)
		{
			return;
		}

		JLabel lblToAdd;

		for (int i = 0; i < week.getMonday().size(); i++)
		{
			lblToAdd = new JLabel(week.getMonday().get(i).getType() + " " + week.getMonday().get(i).getStartTime());
			lblToAdd.setFont(new Font("Tahoma", Font.PLAIN, 24));
			lblToAdd.setBorder(new EmptyBorder(0, 30, 0, 0));
			panel_monday.add(lblToAdd);
		}

		for (int i = 0; i < week.getTuesday().size(); i++)
		{
			lblToAdd = new JLabel(week.getTuesday().get(i).getType() + " " + week.getTuesday().get(i).getStartTime());
			lblToAdd.setFont(new Font("Tahoma", Font.PLAIN, 24));
			lblToAdd.setBorder(new EmptyBorder(0, 30, 0, 0));
			panel_tuesday.add(lblToAdd);
		}

		for (int i = 0; i < week.getWednesday().size(); i++)
		{
			lblToAdd = new JLabel(
			        week.getWednesday().get(i).getType() + " " + week.getWednesday().get(i).getStartTime());
			lblToAdd.setFont(new Font("Tahoma", Font.PLAIN, 24));
			lblToAdd.setBorder(new EmptyBorder(0, 30, 0, 0));
			panel_wednesday.add(lblToAdd);
		}

		for (int i = 0; i < week.getThursday().size(); i++)
		{
			lblToAdd = new JLabel(week.getThursday().get(i).getType() + " " + week.getThursday().get(i).getStartTime());
			lblToAdd.setFont(new Font("Tahoma", Font.PLAIN, 24));
			lblToAdd.setBorder(new EmptyBorder(0, 30, 0, 0));
			panel_thursday.add(lblToAdd);
		}

		for (int i = 0; i < week.getFriday().size(); i++)
		{
			lblToAdd = new JLabel(week.getFriday().get(i).getType() + " " + week.getFriday().get(i).getStartTime());
			lblToAdd.setFont(new Font("Tahoma", Font.PLAIN, 24));
			lblToAdd.setBorder(new EmptyBorder(0, 30, 0, 0));
			panel_friday.add(lblToAdd);
		}

		if (week.getMonday().size() == 0 && week.getTuesday().size() == 0 && week.getWednesday().size() == 0
		        && week.getThursday().size() == 0 && week.getFriday().size() == 0)
		{
			GUI.showWarning("", "Week Is Empty");
		}
	}

	public Employee getEmployee()
	{
		return employee;
	}
}
