package ui;

import java.awt.Button;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import manager.Employee;
import manager.Main;

public class Entry_Screen extends JFrame implements ActionListener
{
	JComboBox<String> actionComboBox = new JComboBox<String>();
	private JPanel buttonPane = new JPanel();

	private JPanel contentPane;

	private Employee employee;

	private ArrayList<manager.Entry> employeesEntries;

	private ArrayList<Button> eventButtons = new ArrayList<Button>();

	/**
	 * Create the frame.
	 */
	public Entry_Screen(Employee employee)
	{
		this.employee = employee;

		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, GUI.screenWidth, GUI.screenHeight);
		GUI.center(this);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		buttonPane.setLayout(new BoxLayout(buttonPane, BoxLayout.Y_AXIS));

		listMeetings();

		JScrollPane scrollPane = new JScrollPane(buttonPane);
		scrollPane.setBounds(346, 139, 609, 432);
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

		scrollPane.validate();
		scrollPane.repaint();

		contentPane.add(scrollPane);

		JLabel lblAllOfYour = new JLabel("All of your meetings");
		lblAllOfYour.setFont(new Font("Tahoma", Font.PLAIN, 32));
		lblAllOfYour.setHorizontalAlignment(SwingConstants.CENTER);
		lblAllOfYour.setBounds(346, 70, 609, 56);
		contentPane.add(lblAllOfYour);

		JButton btnBack = new JButton("Back");
		btnBack.setFont(new Font("Tahoma", Font.PLAIN, 24));
		btnBack.setBounds(12, 688, 214, 64);
		contentPane.add(btnBack);

		JButton btnAddAMeeting = new JButton("Add A Meeting");
		btnAddAMeeting.setFont(new Font("Tahoma", Font.PLAIN, 24));
		btnAddAMeeting.setBounds(34, 304, 255, 56);
		contentPane.add(btnAddAMeeting);

		btnAddAMeeting.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent e)
			{
				GUI.showAddEntry(getEntryScreen());
			}
		});

		actionComboBox.setFont(new Font("Tahoma", Font.PLAIN, 24));
		actionComboBox.setModel(new DefaultComboBoxModel<String>(new String[] { "Delete", "Edit" }));
		actionComboBox.setBounds(623, 584, 332, 69);
		contentPane.add(actionComboBox);

		JButton btnUndo = new JButton("Undo");
		btnUndo.setFont(new Font("Tahoma", Font.PLAIN, 24));
		btnUndo.setBounds(356, 586, 214, 64);
		contentPane.add(btnUndo);

		btnUndo.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent e)
			{

				if (Main.main.getCompany().undoEntryAction())
				{
					listMeetings();
					GUI.showWarning("", "Undo Succesfull");
				} else
				{
					GUI.showWarning("", "Undo Unsuccessful");
				}
			}
		});

		btnBack.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent e)
			{
				GUI.showEmployee(getEmployee());
			}
		});
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		String buttonText = e.getSource().toString();

		for (int i = 0; i < eventButtons.size(); i++)
		{
			if (eventButtons.get(i).toString().equals(buttonText))
			{
				if ((String.valueOf(actionComboBox.getSelectedItem())).equals("Delete")) // DELETE
				{
					Main.main.getCompany().deleteEntry(employeesEntries.get(i).getStartTime(),
					        employeesEntries.get(i).getNumberOfSlots(), employeesEntries.get(i).getDate());

					GUI.showWarning("", "Meeting Deleted");

					listMeetings();
					return;
				} else
				{
					GUI.showEditEntry(getEntryScreen(), employeesEntries.get(i));
					System.out.println(employeesEntries.get(i).toString());
					return;
				}
			}
		}

	}

	public Employee getEmployee()
	{
		return employee;
	}

	public Entry_Screen getEntryScreen()
	{
		return this;
	}

	public void listMeetings()
	{
		employeesEntries = Main.main.getCompany().getEmployeesEntries(getEmployee().getID());

		eventButtons.clear();
		buttonPane.removeAll();

		if (employeesEntries != null)
		{
			for (int i = 0; i < employeesEntries.size(); i++)
			{
				eventButtons.add(new Button(employeesEntries.get(i).toString()));
				eventButtons.get(i).setFont(new Font("Tahoma", Font.PLAIN, 24));
				buttonPane.setBorder(new EmptyBorder(5, 5, 5, 5));
				buttonPane.add(eventButtons.get(i));

				// ADD DELETE ON PRESS
				eventButtons.get(i).addActionListener(this);

				buttonPane.add(Box.createRigidArea(new Dimension(5, 5)));
			}
		}

		buttonPane.revalidate();
		buttonPane.repaint();
	}
}
