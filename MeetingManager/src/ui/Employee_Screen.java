package ui;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import manager.Employee;
import manager.Main;

public class Employee_Screen extends JFrame
{
	private JPanel contentPane;

	private Employee employee;

	/**
	 * Create the frame.
	 */
	public Employee_Screen(Employee employee)
	{
		this.employee = employee;
		setResizable(false);
		setTitle("Employee Menu");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, GUI.screenWidth, GUI.screenHeight);
		GUI.center(this);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblWelcomeLabel = new JLabel("Welcome");
		lblWelcomeLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblWelcomeLabel.setFont(new Font("Tahoma", Font.PLAIN, 32));
		lblWelcomeLabel.setBounds(12, 105, 970, 64);
		contentPane.add(lblWelcomeLabel);

		JButton btnAddAnEvent = new JButton("Manage Events");
		btnAddAnEvent.setFont(new Font("Tahoma", Font.PLAIN, 24));
		btnAddAnEvent.setBounds(304, 257, 401, 99);
		contentPane.add(btnAddAnEvent);

		JButton btnViewSchedule = new JButton("View Schedule");
		btnViewSchedule.setFont(new Font("Tahoma", Font.PLAIN, 24));
		btnViewSchedule.setBounds(304, 452, 401, 99);
		contentPane.add(btnViewSchedule);

		JButton btnLogOut = new JButton("Log Out");
		btnLogOut.setFont(new Font("Tahoma", Font.PLAIN, 24));
		btnLogOut.setBounds(12, 688, 214, 64);
		contentPane.add(btnLogOut);

		btnAddAnEvent.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent e)
			{
				GUI.showEntry(getEmployee());
			}
		});

		btnViewSchedule.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent e)
			{
				GUI.showWeekSelect(getEmployee());
			}
		});

		btnLogOut.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent e)
			{
				Main.main.logOut();
				GUI.showMenu();
			}
		});

		if (Main.main.loggedIn() != null)
		{
			lblWelcomeLabel.setText("Welcome " + employee.getName() + ". ID " + employee.getID());
		}
	}

	public Employee getEmployee()
	{
		return employee;
	}
}
