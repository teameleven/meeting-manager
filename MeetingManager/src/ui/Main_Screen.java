package ui;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import manager.SaveManager;

public class Main_Screen extends JFrame
{

	private JPanel contentPane;

	/**
	 * Create the frame.
	 */
	public Main_Screen()
	{
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, GUI.screenWidth, GUI.screenHeight);
		GUI.center(this);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton btnEmployeeLogin = new JButton("Employee Login");
		btnEmployeeLogin.setFont(new Font("Tahoma", Font.PLAIN, 32));
		btnEmployeeLogin.setBounds(83, 241, 764, 100);
		contentPane.add(btnEmployeeLogin);

		btnEmployeeLogin.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				GUI.showLogin();
			}
		});

		JLabel lblMeetingManager = new JLabel("Meeting Manager");
		lblMeetingManager.setFont(new Font("Tahoma", Font.PLAIN, 48));
		lblMeetingManager.setHorizontalAlignment(SwingConstants.CENTER);
		lblMeetingManager.setBounds(12, 77, 958, 100);
		contentPane.add(lblMeetingManager);

		JButton btnSaveData = new JButton("Save Data");
		btnSaveData.setFont(new Font("Tahoma", Font.PLAIN, 32));
		btnSaveData.setBounds(154, 532, 302, 100);
		contentPane.add(btnSaveData);

		btnSaveData.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent arg0)
			{
				SaveManager.saveData();
			}
		});

		JButton btnLoadData = new JButton("Load Data");
		btnLoadData.setFont(new Font("Tahoma", Font.PLAIN, 32));
		btnLoadData.setBounds(495, 532, 302, 100);
		contentPane.add(btnLoadData);

		JButton btnEmployeeRegister = new JButton("Employee Register");
		btnEmployeeRegister.setFont(new Font("Tahoma", Font.PLAIN, 32));
		btnEmployeeRegister.setBounds(83, 387, 764, 100);
		contentPane.add(btnEmployeeRegister);

		btnEmployeeRegister.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent e)
			{
				GUI.showRegister();
			}
		});

		btnLoadData.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent e)
			{
				SaveManager.openData();
			}
		});
	}
}
