package ui;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import manager.Employee;
import manager.Entry;

public class GUI
{
	static Add_Entry_Screen screenAddEntry;
	static Edit_Screen screenEditEntry;

	static Employee_Screen screenEmployee;
	static Entry_Screen screenEntry;
	public static final int screenHeight = 800;
	static Login_Screen screenLogin = new Login_Screen();
	static Main_Screen screenMain = new Main_Screen();
	static Register_Screen screenRegister;
	static Schedule_View screenSchedule;
	static Week_Select_Screen screenWeekSelect;
	public static final int screenWidth = 1000;

	public static void center(JFrame frame)
	{
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		frame.setLocation(dim.width / 2 - frame.getSize().width / 2, dim.height / 2 - frame.getSize().height / 2);
	}

	private static void closeAll()
	{
		if (screenMain != null)
		{
			screenMain.setVisible(false);
		}
		if (screenLogin != null)
		{
			screenLogin.setVisible(false);
		}
		if (screenEmployee != null)
		{
			screenEmployee.setVisible(false);
		}
		if (screenSchedule != null)
		{
			screenSchedule.setVisible(false);
		}
		if (screenEntry != null)
		{
			screenEntry.setVisible(false);
		}
	}

	public static void showAddEntry(Entry_Screen parent)
	{
		screenAddEntry = new Add_Entry_Screen(parent);
		screenAddEntry.setVisible(true);
	}

	public static void showEditEntry(Entry_Screen parent, Entry entry)
	{
		screenEditEntry = new Edit_Screen(parent, entry);
		screenEditEntry.setVisible(true);
	}

	public static void showEmployee(Employee employee)
	{
		screenEmployee = new Employee_Screen(employee);
		closeAll();
		screenEmployee.setVisible(true);
	}

	public static void showEntry(Employee employee)
	{
		screenEntry = new Entry_Screen(employee);
		closeAll();
		screenEntry.setVisible(true);
	}

	public static void showLogin()
	{
		closeAll();
		screenLogin.setVisible(true);
	}

	public static void showMenu()
	{
		closeAll();
		screenMain.setVisible(true);
	}

	public static void showRegister()
	{
		screenRegister = new Register_Screen();
		screenRegister.setVisible(true);
	}

	public static void showSchedule(Employee employee, String date)
	{
		screenSchedule = new Schedule_View(employee, date);
		closeAll();
		screenSchedule.setVisible(true);
	}

	public static void showWarning(String title, String text)
	{
		JOptionPane.showConfirmDialog(null, text, title, JOptionPane.CLOSED_OPTION, JOptionPane.WARNING_MESSAGE);
	}

	public static void showWeekSelect(Employee employee)
	{
		screenWeekSelect = new Week_Select_Screen(employee);
		screenWeekSelect.setVisible(true);
	}

}
