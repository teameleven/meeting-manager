package ui;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.text.NumberFormat;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.text.NumberFormatter;

import manager.Main;
import manager.Time;

public class Add_Entry_Screen extends JFrame implements ActionListener
{

	JPanel buttonPane = new JPanel();
	private JPanel contentPane;
	private ArrayList<Button> eventButtons = new ArrayList<Button>();
	private ArrayList<Integer> IDs = new ArrayList<Integer>();
	private Entry_Screen parentScreen;

	/**
	 * Create the frame.
	 */
	public Add_Entry_Screen(Entry_Screen parentScreen)
	{
		setTitle("Add Meeting");
		this.parentScreen = parentScreen;

		setResizable(false);
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 600, 400);
		GUI.center(this);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		NumberFormat format = NumberFormat.getIntegerInstance();
		format.setGroupingUsed(false);
		NumberFormatter formatter = new NumberFormatter(format);
		formatter.setValueClass(Integer.class);
		formatter.setMinimum(-1);
		formatter.setMaximum(99999);
		formatter.setAllowsInvalid(false);
		// If you want the value to be committed on each keystroke instead of focus lost
		formatter.setCommitsOnValidEdit(true);

		JPanel panel = new JPanel();
		panel.setBorder(null);
		panel.setBounds(356, 88, 238, 281);
		contentPane.add(panel);
		panel.setLayout(null);

		JLabel lblStartDate = new JLabel("Start Date (D/M/Y)");
		lblStartDate.setBounds(12, 13, 226, 29);
		panel.add(lblStartDate);
		lblStartDate.setHorizontalAlignment(SwingConstants.CENTER);
		lblStartDate.setFont(new Font("Tahoma", Font.PLAIN, 24));

		JSpinner spinner_day = new JSpinner();
		spinner_day.setModel(new SpinnerNumberModel(1, 1, 31, 1));
		spinner_day.setFont(new Font("Tahoma", Font.PLAIN, 24));
		spinner_day.setBounds(12, 55, 60, 30);
		panel.add(spinner_day);

		JSpinner spinner_month = new JSpinner();
		spinner_month.setModel(new SpinnerNumberModel(1, 1, 12, 1));
		spinner_month.setFont(new Font("Tahoma", Font.PLAIN, 24));
		spinner_month.setBounds(84, 55, 60, 30);
		panel.add(spinner_month);

		JSpinner spinner_year = new JSpinner();
		spinner_year.setModel(new SpinnerNumberModel(18, 18, 21, 1));
		spinner_year.setFont(new Font("Tahoma", Font.PLAIN, 24));
		spinner_year.setBounds(156, 55, 60, 30);
		panel.add(spinner_year);

		JLabel lblStartTimedmy = new JLabel("Start Time (H/M)");
		lblStartTimedmy.setHorizontalAlignment(SwingConstants.CENTER);
		lblStartTimedmy.setFont(new Font("Tahoma", Font.PLAIN, 24));
		lblStartTimedmy.setBounds(12, 112, 226, 29);
		panel.add(lblStartTimedmy);

		JSpinner spinner_hour = new JSpinner();
		spinner_hour.setModel(new SpinnerNumberModel(9, 9, 16, 1));
		spinner_hour.setFont(new Font("Tahoma", Font.PLAIN, 24));
		spinner_hour.setBounds(53, 154, 60, 30);
		panel.add(spinner_hour);

		JSpinner spinner_minute = new JSpinner();
		spinner_minute.setModel(new SpinnerNumberModel(0, 0, 59, 15));
		spinner_minute.setFont(new Font("Tahoma", Font.PLAIN, 24));
		spinner_minute.setBounds(125, 154, 60, 30);
		panel.add(spinner_minute);

		JComboBox<String> comboBox = new JComboBox<String>();
		comboBox.setBounds(12, 212, 214, 41);
		panel.add(comboBox);
		comboBox.setFont(new Font("Tahoma", Font.PLAIN, 24));
		comboBox.setModel(new DefaultComboBoxModel<String>(
		        new String[] { "Regular Meeting", "Project Meeting", "Presentation", "Client Meeting" }));

		JButton btnAddMeeting = new JButton("Add Meeting");
		btnAddMeeting.setFont(new Font("Tahoma", Font.PLAIN, 24));
		btnAddMeeting.setBounds(12, 284, 560, 58);
		panel.add(btnAddMeeting);

		buttonPane.setBounds(255, 127, 317, 144);
		panel.add(buttonPane);
		buttonPane.setLayout(new BoxLayout(buttonPane, BoxLayout.Y_AXIS));

		JLabel lblAddEmployees = new JLabel("Add Employees (Enter)");
		lblAddEmployees.setHorizontalAlignment(SwingConstants.CENTER);
		lblAddEmployees.setFont(new Font("Tahoma", Font.PLAIN, 24));
		lblAddEmployees.setBounds(258, 13, 280, 29);
		panel.add(lblAddEmployees);
		JTextField textField = new JFormattedTextField(formatter);
		panel.add(textField);

		textField.setFont(new Font("Tahoma", Font.PLAIN, 24));
		textField.setBounds(250, 56, 245, 58);
		textField.setColumns(10);

		// catch the enter key in the JTextField and perform an OK click on the JButton
		textField.addKeyListener(new KeyAdapter()
		{
			@Override
			public void keyPressed(KeyEvent e)
			{
				if (e.getKeyCode() == KeyEvent.VK_ENTER)
				{
					try
					{
						if (Main.main.getCompany().doesEmployeeExist(Integer.parseInt(textField.getText().toString())))
						{
							int IDtoAdd = Integer.parseInt(textField.getText().toString());

							if (IDtoAdd == getParent().getEmployee().getID())
							{
								GUI.showWarning("", "Cannot Add Yourself");
							} else if (IDs.contains(IDtoAdd))
							{
								GUI.showWarning("", "Employee Already Exists");
							} else
							{
								IDs.add(IDtoAdd);
								formatter.setAllowsInvalid(true);
								textField.setText("");
								formatter.setAllowsInvalid(false);
								listIDs();
							}

						} else
						{
							// TOAST
						}
					} catch (Exception exception)
					{

					}
				}
			}
		});

		JButton btnDel = new JButton("DEL");
		btnDel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnDel.setBounds(507, 55, 65, 59);
		panel.add(btnDel);

		btnDel.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent e)
			{
				formatter.setAllowsInvalid(true);
				textField.setText("");
				formatter.setAllowsInvalid(false);
			}
		});

		btnAddMeeting.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent e)
			{
				if (!IDs.contains(getParent().getEmployee().getID()))
				{
					IDs.add(getParent().getEmployee().getID());
				}

				int[] employeeIDs = new int[IDs.size()];

				for (int i = 0; i < employeeIDs.length; i++)
				{
					employeeIDs[i] = IDs.get(i);
				}

				String hourPass = (spinner_hour.getValue().toString().length() < 2)
				        ? "0" + spinner_hour.getValue().toString()
				        : spinner_hour.getValue().toString();
				String minutePass = (spinner_minute.getValue().toString().length() < 2)
				        ? "0" + spinner_minute.getValue().toString()
				        : spinner_minute.getValue().toString();

				if (Main.main.getCompany().addEntry(new Time(hourPass + minutePass), 1,
				        comboBox.getSelectedItem().toString(), (spinner_day.getValue().toString() + "/"
				                + spinner_month.getValue().toString() + "/" + spinner_year.getValue().toString()),
				        employeeIDs))
				{
					GUI.showWarning("", "Meeting Add Successful");
					dispose();
				} else
				{
					GUI.showWarning("", "Meeting Add Failed");
				}

				getParent().listMeetings();
				IDs.clear();
			}
		});
	}

	@Override
	public void actionPerformed(ActionEvent e)
	{
		String buttonText = e.getSource().toString();
		int IDtoRemove = 0;

		for (int i = 0; i < eventButtons.size(); i++)
		{
			if (eventButtons.get(i).toString().equals(buttonText))
			{
				IDtoRemove = Integer.parseInt(eventButtons.get(i).getLabel());
			}
		}

		IDs.remove((Integer) IDtoRemove);

		GUI.showWarning("", "Employee Deleted");

		listIDs();
	}

	@Override
	public Entry_Screen getParent()
	{
		return parentScreen;
	}

	public void listIDs()
	{
		eventButtons.clear();
		buttonPane.removeAll();

		for (int i = 0; i < IDs.size(); i++)
		{
			eventButtons.add(new Button(IDs.get(i).toString()));
			eventButtons.get(i).setFont(new Font("Tahoma", Font.PLAIN, 24));
			buttonPane.setBorder(new EmptyBorder(5, 5, 5, 5));
			buttonPane.add(eventButtons.get(i));

			// ADD DELETE ON PRESS
			eventButtons.get(i).addActionListener(this);

			buttonPane.add(Box.createRigidArea(new Dimension(5, 5)));
		}

		buttonPane.revalidate();
		buttonPane.repaint();
	}
}
