package ui;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import manager.Employee;

public class Week_Select_Screen extends JFrame
{
	private JPanel contentPane;
	private Employee employee;

	/**
	 * Create the frame.
	 */
	public Week_Select_Screen(Employee employee)
	{
		this.employee = employee;

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		GUI.center(this);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblStartDate = new JLabel("Date (D/M/Y)");
		lblStartDate.setBounds(90, 40, 226, 29);
		lblStartDate.setHorizontalAlignment(SwingConstants.CENTER);
		lblStartDate.setFont(new Font("Tahoma", Font.PLAIN, 24));
		contentPane.add(lblStartDate);

		JSpinner spinner_day = new JSpinner();
		spinner_day.setModel(new SpinnerNumberModel(1, 1, 31, 1));
		spinner_day.setFont(new Font("Tahoma", Font.PLAIN, 24));
		spinner_day.setBounds(90, 82, 60, 30);
		contentPane.add(spinner_day);

		JSpinner spinner_month = new JSpinner();
		spinner_month.setModel(new SpinnerNumberModel(1, 1, 12, 1));
		spinner_month.setFont(new Font("Tahoma", Font.PLAIN, 24));
		spinner_month.setBounds(173, 82, 60, 30);
		contentPane.add(spinner_month);

		JSpinner spinner_year = new JSpinner();
		spinner_year.setModel(new SpinnerNumberModel(18, 18, 21, 1));
		spinner_year.setFont(new Font("Tahoma", Font.PLAIN, 24));
		spinner_year.setBounds(256, 82, 60, 30);
		contentPane.add(spinner_year);

		JButton btnShowSchedule = new JButton("Show Schedule");
		btnShowSchedule.setFont(new Font("Tahoma", Font.PLAIN, 24));
		btnShowSchedule.setBounds(87, 151, 248, 55);
		contentPane.add(btnShowSchedule);

		btnShowSchedule.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent e)
			{
				dispose();
				GUI.showSchedule(getEmployee(), (spinner_day.getValue().toString() + "/"
				        + spinner_month.getValue().toString() + "/" + spinner_year.getValue().toString()));
			}
		});

	}

	public Employee getEmployee()
	{
		return employee;
	}
}
