package manager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Stack;

/**
 * This class handles all of the data required for the Company. This includes a
 * list of employees, a list of all meetings going on and a stack of entry
 * actions. For these requirements, we have an ArrayList of Employees, an
 * ArrayList of Entries and a Stack of Entry Actions.
 */
public class Company
{
	ArrayList<Employee> employees;
	Stack<EntryAction> entryActionStack;
	ArrayList<Entry> meetingDiary;

	public Company()
	{
		employees = new ArrayList<Employee>();
		entryActionStack = new Stack<EntryAction>();
		meetingDiary = new ArrayList<Entry>();

	}

	/**
	 * This method adds an Employee to the ArrayList of Employees.
	 * 
	 * @param employee
	 */
	public boolean addEmployee(Employee employee)
	{
		employees.add(employee);
		return true;
	}

	/**
	 * Constructs a new Entry object, taking in the parameters as it's data, and
	 * adds it to the meetingDiary. The method will then create a new EntryAction
	 * object, passing the added Entry and setting the Type to add, then pushing it
	 * onto the entryActionStack
	 * 
	 * @param startTime
	 * @param numberOfSlots
	 * @param type
	 * @param date
	 * @param employeeIDs
	 */
	public boolean addEntry(Time startTime, int numberOfSlots, String type, String date, int[] employeeIDs)
	{
		if (checkIfDuplicateEntryExist(startTime, numberOfSlots, date))
		{
			return false;
		}
		Entry entry = new Entry(startTime, numberOfSlots, type, date, employeeIDs);

		meetingDiary.add(entry);

		EntryAction thisEntryAction = new EntryAction(entry);
		thisEntryAction.setTypeToAdd();
		entryActionStack.push(thisEntryAction);
		return true;
	}

	/**
	 * Checks if there is a duplicate meeting, so no 2 meetings clash in timetable
	 * using fields of entry
	 * 
	 * @param startTime
	 *            used to see if the 2 meetings start at same time
	 * @param numberOfSlots
	 *            used to see if the meeting length is same
	 * @param date
	 *            used to see if both meetings happen at the same date
	 * @return boolean value false, meaning there isn't duplicate
	 */
	public boolean checkIfDuplicateEntryExist(Time startTime, int numberOfSlots, String date)
	{
		for (int i = 0; i < meetingDiary.size(); i++)
		{
			// if the conditions all match up means, entry can be used
			if (startTime.getHour() == meetingDiary.get(i).getStartTime().getHour()
			        && startTime.getMinute() == meetingDiary.get(i).getStartTime().getMinute()
			        && numberOfSlots == meetingDiary.get(i).getNumberOfSlots()
			        && date.equals(meetingDiary.get(i).getDate()))
			{
				return true; // entry does exist, duplicate exist
			}
		}
		return false;

	}

	/**
	 * This method receives a Date object as a parameter and converts it to a String
	 * variable. This method is primarily used for the getWeek() methods
	 * 
	 * @param date
	 * @return
	 */
	public String convertDateToString(Date date)
	{
		String date1 = null;
		try
		{
			date1 = new SimpleDateFormat("d/M/yy").format(date);
		} catch (Exception ex)
		{
			System.out.print(ex);
		}

		return date1;
	}

	/**
	 * Converts a String value to a date
	 * 
	 * @param date
	 * @return
	 */
	public Date convertStringToDate(String date)
	{

		Date date1 = null;
		try
		{
			date1 = new SimpleDateFormat("d/M/yy").parse(date);
		} catch (ParseException e)
		{

			e.printStackTrace();
		}
		return date1;

	}

	/**
	 * Searches for an Entry object within the meetingDiary, and deletes it if the
	 * Entry exists. The method will then create a new EntryAction object, passing
	 * the deleted Entry and setting the Type to delete, then pushing it onto the
	 * entryActionStack
	 * 
	 * @param searchStartTime
	 * @param searchNumberOfSlots
	 * @param searchDate
	 */
	public void deleteEntry(Time searchStartTime, int searchNumberOfSlots, String searchDate)
	{
		Entry entry = null;
		for (int i = 0; i < meetingDiary.size(); i++)
		{
			if (searchStartTime == meetingDiary.get(i).getStartTime()
			        && searchNumberOfSlots == meetingDiary.get(i).getNumberOfSlots()
			        && searchDate.equals(meetingDiary.get(i).getDate()))
			{
				entry = meetingDiary.get(i);
				meetingDiary.remove(i);
			}
		}

		if (entry != null)
		{
			EntryAction thisEntryAction = new EntryAction(entry);
			thisEntryAction.setTypeToDelete();
			entryActionStack.push(thisEntryAction);
		}
	}

	/**
	 * This method searches for an Employee within the Employee Array List, similar
	 * to the findEmployee() method except it returns a boolean value instead.
	 * 
	 * @param ID
	 *            used to compare each member of the Employee Array List.
	 * @return exists true if the Employee exists, false if the Employee does not.
	 */
	public boolean doesEmployeeExist(int ID)
	{
		boolean exists = false;

		for (int i = 0; i < employees.size(); i++)
		{
			if (employees.get(i).getID() == ID)
			{
				exists = true;
			}
		}
		return exists;
	}

	/**
	 * Searches for an Entry object within the meetingDiary using the parameters
	 * passed as search criteria. Creates a new entry matching the one just found.
	 * Then creates an editedEntry by copying the contents of the Entry that was
	 * searched for. This editedEntry is then edited based on the parameters passed
	 * and adds it to the meetingDiary and removes the Entry that was searched for.
	 * The method will then create a new EntryAction object, passing the deleted
	 * Entry, the edited Entry and setting the Type to edit, then pushing it onto
	 * the entryActionStack
	 * 
	 * @param searchStartTime
	 *            search term
	 * @param searchNumberOfSlots
	 *            search term
	 * @param searchDate
	 *            search term
	 * @param startTime
	 *            data for the entry to be edited with
	 * @param numberOfSlots
	 *            data for the entry to be edited with
	 * @param type
	 *            data for the entry to be edited with
	 * @param date
	 *            data for the entry to be edited with
	 * @param employeeIDs
	 *            data for the entry to be edited with
	 */

	public void editEntry(Time searchStartTime, int searchNumberOfSlots, String searchDate, Time startTime,
	        int numberOfSlots, String type, String date, int[] employeeIDs)
	{
		Entry entry = null;
		Entry editedEntry = null;
		for (int i = 0; i < meetingDiary.size(); i++)
		{
			if (searchStartTime.getHour() == meetingDiary.get(i).getStartTime().getHour()
			        && searchStartTime.getMinute() == meetingDiary.get(i).getStartTime().getMinute()
			        && searchNumberOfSlots == meetingDiary.get(i).getNumberOfSlots()
			        && searchDate.equals(meetingDiary.get(i).getDate()))
			{
				entry = meetingDiary.get(i);
				meetingDiary.remove(i);
				break;
			}
		}

		if (entry != null)
		{
			editedEntry = new Entry(entry.getStartTime(), entry.getNumberOfSlots(), entry.getType(), entry.getDate(),
			        entry.getEmployeeIDs());
			
			editedEntry.setStartTime(startTime);
			editedEntry.setDate(date);
			editedEntry.setNumberOfSlots(numberOfSlots);
			editedEntry.setType(type);
			editedEntry.setEmployeeIDs(employeeIDs);
			
			meetingDiary.add(editedEntry);
			EntryAction thisEntryAction = new EntryAction(editedEntry, entry);
			thisEntryAction.setTypeToEdit();
			entryActionStack.push(thisEntryAction);
		}
	}

	/**
	 * This method searches for an Employee within the Employee Array List.
	 * 
	 * @param ID
	 *            used as a comparison each member of the Employee Array List
	 * @return Employee returns the Employee object if they are in the Employee
	 *         Array List, or null if they are not.
	 */
	public Employee findEmployee(int ID)
	{
		for (int i = 0; i < employees.size(); i++)
		{
			if (employees.get(i).getID() == ID)
			{
				return employees.get(i);
			}
		}

		return null;
	}

	/**
	 * Returns all the meetings for the specified Employee ID
	 * 
	 * @param ID
	 *            used to compare with ID in employee array
	 * @return
	 */
	public ArrayList<Entry> getEmployeesEntries(int ID)
	{
		ArrayList<Entry> employeesEntries = new ArrayList<Entry>();
		int[] employeeIDs;

		for (int i = 0; i < meetingDiary.size(); i++)
		{
			employeeIDs = meetingDiary.get(i).getEmployeeIDs();

			for (int j = 0; j < employeeIDs.length; j++)
			{
				if (employeeIDs[j] == ID)
				{
					employeesEntries.add(meetingDiary.get(i));
				}
			}
		}

		return employeesEntries;
	}

	public String getStartAndEndOfWeek(String date)
	{
		Date dDate = new Date();
		dDate = convertStringToDate(date);
		Calendar c = Calendar.getInstance();
		c.setTime(dDate);
		int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
		String startDate;
		String endDate;

		if (dayOfWeek == 2)
		{
			startDate = date;
			c.add(Calendar.DATE, 4);
			dDate = c.getTime();
			endDate = convertDateToString(dDate);
			String week = startDate + "-" + endDate;
			return week;

		} else if (dayOfWeek == 3)
		{
			c.add(Calendar.DATE, -1);
			dDate = c.getTime();
			startDate = convertDateToString(dDate);
			c.add(Calendar.DATE, 4);
			dDate = c.getTime();
			endDate = convertDateToString(dDate);

			String week = startDate + "-" + endDate;
			return week;
		} else if (dayOfWeek == 4)
		{
			c.add(Calendar.DATE, -2);
			dDate = c.getTime();
			startDate = convertDateToString(dDate);
			c.add(Calendar.DATE, 4);
			dDate = c.getTime();
			endDate = convertDateToString(dDate);

			String week = startDate + "-" + endDate;
			return week;
		} else if (dayOfWeek == 5)
		{
			c.add(Calendar.DATE, -3);
			dDate = c.getTime();
			startDate = convertDateToString(dDate);
			c.add(Calendar.DATE, 4);
			dDate = c.getTime();
			endDate = convertDateToString(dDate);

			String week = startDate + "-" + endDate;
			return week;
		} else if (dayOfWeek == 6)
		{
			c.add(Calendar.DATE, -4);
			dDate = c.getTime();
			startDate = convertDateToString(dDate);
			c.add(Calendar.DATE, 4);
			dDate = c.getTime();
			endDate = convertDateToString(dDate);

			String week = startDate + "-" + endDate;
			return week;
		} else
		{
			return null;
		}

	}

	/**
	 * This method will gather data for an Employee's week and return it. Firstly a
	 * Week object called week is created along with a ArrayList of entries called
	 * tempDay and an array of EmployeeIDs called tempEmployeeIDs. The method loops
	 * 5 times, once for each day of the week. For the first loop, it will find the
	 * details for the ArrayList monday in the Week class. Firstly, the method will
	 * go through the meetingDiary and search for Entries that have the searched for
	 * the required date and the ID of the employee that we are looking for. If an
	 * entry matches, it will be added to tempDay. Then the method setMonday is
	 * called, passing the tempDay data. Then on the next loop, the date will be
	 * incremented by 1, and will repeat the same process except for the next day.
	 * This is done until all days of the week have data in them and returns the
	 * entire week.
	 * 
	 * @param ID
	 * @param startDate
	 * @param endDate
	 * @return Week
	 * @throws Exception
	 */
	public Week getWeek(int ID, String weekDates) throws Exception
	{
		String startDate;
		String endDate;
		String[] weekParts = weekDates.split("-");

		startDate = weekParts[0];
		endDate = weekParts[1];

		Week week = new Week();
		ArrayList<Entry> tempDay = new ArrayList<Entry>();
		int[] tempEmployeeIDs = null;

		Date dStartDate = convertStringToDate(startDate);

		for (int i = 0; i < 5; i++)
		{
			if (i == 0)
			{
				for (int j = 0; j < meetingDiary.size(); j++)
				{
					if (startDate.equals(meetingDiary.get(j).getDate()))
					{
						tempEmployeeIDs = meetingDiary.get(j).getEmployeeIDs();
						for (int k = 0; k < tempEmployeeIDs.length; k++)
						{
							if (ID == tempEmployeeIDs[k])
							{
								tempDay.add(meetingDiary.get(j));
							}
						}
					}
				}

				week.setMonday(tempDay);
			} else if (i == 1)
			{
				Date dt = new Date(); //
				dt = dStartDate;//
				Calendar c = Calendar.getInstance(); // increments date by 1
				c.setTime(dt);//
				c.add(Calendar.DATE, 1);//
				dt = c.getTime();//
				dStartDate = dt;//
				startDate = convertDateToString(dStartDate);

				for (int j = 0; j < meetingDiary.size(); j++)
				{
					if (startDate.equals(meetingDiary.get(j).getDate()))
					{
						tempEmployeeIDs = meetingDiary.get(j).getEmployeeIDs();
						for (int k = 0; k < tempEmployeeIDs.length; k++)
						{
							if (ID == tempEmployeeIDs[k])
							{
								tempDay.add(meetingDiary.get(j));
							}
						}
					}
				}
				week.setTuesday(tempDay);
			} else if (i == 2)
			{
				Date dt = new Date(); //
				dt = dStartDate;//
				Calendar c = Calendar.getInstance(); // increments date by 1
				c.setTime(dt);//
				c.add(Calendar.DATE, 1);//
				dt = c.getTime();//
				dStartDate = dt;//
				startDate = convertDateToString(dStartDate);

				for (int j = 0; j < meetingDiary.size(); j++)
				{
					if (startDate.equals(meetingDiary.get(j).getDate()))
					{
						tempEmployeeIDs = meetingDiary.get(j).getEmployeeIDs();
						for (int k = 0; k < tempEmployeeIDs.length; k++)
						{
							if (ID == tempEmployeeIDs[k])
							{
								tempDay.add(meetingDiary.get(j));
							}
						}
					}
				}
				week.setWednesday(tempDay);
			} else if (i == 3)
			{
				Date dt = new Date(); //
				dt = dStartDate;//
				Calendar c = Calendar.getInstance(); // increments date by 1
				c.setTime(dt);//
				c.add(Calendar.DATE, 1);//
				dt = c.getTime();//
				dStartDate = dt;//
				startDate = convertDateToString(dStartDate);

				for (int j = 0; j < meetingDiary.size(); j++)
				{
					if (startDate.equals(meetingDiary.get(j).getDate()))
					{
						tempEmployeeIDs = meetingDiary.get(j).getEmployeeIDs();
						for (int k = 0; k < tempEmployeeIDs.length; k++)
						{
							if (ID == tempEmployeeIDs[k])
							{
								tempDay.add(meetingDiary.get(j));
							}
						}
					}
				}
				week.setThursday(tempDay);
			} else if (i == 4)
			{
				for (int j = 0; j < meetingDiary.size(); j++)
				{
					if (endDate.equals(meetingDiary.get(j).getDate()))
					{
						tempEmployeeIDs = meetingDiary.get(j).getEmployeeIDs();
						for (int k = 0; k < tempEmployeeIDs.length; k++)
						{
							if (ID == tempEmployeeIDs[k])
							{
								tempDay.add(meetingDiary.get(j));
							}
						}
					}
				}
				week.setFriday(tempDay);
			}
			tempDay.clear();
		}
		week.setStartDate(startDate);
		week.setEndDate(endDate);
		return week;
	}

	/**
	 * This method will gather data for the specified week and return it. Firstly a
	 * Week object called week is created along with a ArrayList of entries called
	 * tempDay. The method loops 5 times, once for each day of the week. For the
	 * first loop, it will find the details for the ArrayList Monday in the Week
	 * class. Firstly, the method will go through the meetingDiary and search for
	 * Entries that have the searched for the required date. If an entry matches, it
	 * will be added to tempDay. Then the method setMonday is called, passing the
	 * tempDay data. Then on the next loop, the date will be incremented by 1, and
	 * will repeat the same process except for the next day. This is done until all
	 * days of the week have data in them and returns the entire week.
	 * 
	 * @param startDate
	 * @param endDate
	 * @return Week
	 * @throws Exception
	 */
	public Week getWeek(String weekDates) throws Exception
	{
		String startDate;
		String endDate;
		String[] weekParts = weekDates.split("-");

		startDate = weekParts[0];
		endDate = weekParts[1];

		Week week = new Week();
		ArrayList<Entry> tempDay = new ArrayList<Entry>();

		for (int i = 0; i < 5; i++)
		{
			if (i == 0)
			{
				for (int j = 0; j < meetingDiary.size(); j++)
				{
					if (startDate.equals(meetingDiary.get(j).getDate()))
					{
						tempDay.add(meetingDiary.get(j));
					}
				}
				week.setMonday(tempDay);
			} else if (i == 1)
			{
				Date dStartDate = convertStringToDate(startDate);
				Date dt = new Date(); //
				dt = dStartDate;//
				Calendar c = Calendar.getInstance(); // increments date by 1
				c.setTime(dt);//
				c.add(Calendar.DATE, 1);//
				dt = c.getTime();//
				dStartDate = dt;//
				startDate = convertDateToString(dStartDate);

				for (int j = 0; j < meetingDiary.size(); j++)
				{
					if (startDate.equals(meetingDiary.get(j).getDate()))
					{
						tempDay.add(meetingDiary.get(j));
					}
				}

				week.setTuesday(tempDay);
			} else if (i == 2)
			{
				Date dStartDate = convertStringToDate(startDate);
				Date dt = new Date(); //
				dt = dStartDate;//
				Calendar c = Calendar.getInstance(); // increments date by 1
				c.setTime(dt);//
				c.add(Calendar.DATE, 1);//
				dt = c.getTime();//
				dStartDate = dt;//
				startDate = convertDateToString(dStartDate);

				for (int j = 0; j < meetingDiary.size(); j++)
				{
					if (startDate.equals(meetingDiary.get(j).getDate()))
					{
						tempDay.add(meetingDiary.get(j));
					}
				}
				week.setWednesday(tempDay);
			} else if (i == 3)
			{
				Date dStartDate = convertStringToDate(startDate);
				Date dt = new Date(); //
				dt = dStartDate;//
				Calendar c = Calendar.getInstance(); // increments date by 1
				c.setTime(dt);//
				c.add(Calendar.DATE, 1);//
				dt = c.getTime();//
				dStartDate = dt;//
				startDate = convertDateToString(dStartDate);

				for (int j = 0; j < meetingDiary.size(); j++)
				{
					if (startDate.equals(meetingDiary.get(j).getDate()))
					{
						tempDay.add(meetingDiary.get(j));
					}
				}
				week.setThursday(tempDay);
			} else if (i == 4)
			{
				for (int j = 0; j < meetingDiary.size(); j++)
				{
					if (endDate.equals(meetingDiary.get(j).getDate()))
					{
						tempDay.add(meetingDiary.get(j));
					}
				}
				week.setFriday(tempDay);
			}
			tempDay.clear();
		}

		return week;
	}

	/**
	 * Goes through the entire meetingDiary and print off all of the stored data.
	 */
	public void printEntries()
	{
		if (!meetingDiary.isEmpty())
		{
			for (int i = 0; i < meetingDiary.size(); i++)
			{
				System.out.println(meetingDiary.get(i).getDate() + " " + meetingDiary.get(i).getStartTime() + " "
				        + meetingDiary.get(i).getNumberOfSlots() + " " + meetingDiary.get(i).getType());
			}
		} else
		{
			System.out.println("meeting diary is empty");
		}
	}

	/**
	 * Receives a name and ID as parameters and called doesEmployeeExist passing ID
	 * as a parameter If the employee exists - doesEmployeeExist == true, it will
	 * add employee and return true else return false
	 * 
	 * @param name
	 * @param ID
	 * @return
	 */
	public boolean registerEmployee(String name, int ID)
	{
		if (doesEmployeeExist(ID) == false)
		{
			addEmployee(new Employee(name, ID));
			return true;
		} else
		{
			return false;
		}
	}

	/**
	 * This method will undo the last action taken. This is done by first receiving
	 * the top of the entryActionStack. The method will then create two entries that
	 * copy the data from the top. The method will receive the Type of EntryAction.
	 * If the Type is add, then the method will receive the search data from the top
	 * stack. The method will search through the meetingDiary and remove the entry
	 * that matches the entry stored in the top stack. If the Type is delete, then
	 * the method will simply add the entry from the top stack. If the Type is
	 * delete, then the method will receive search data from the top stack, search
	 * through the meetingDiary, remove the entry matching the editedEntry and add
	 * the originalEntry to the meetingDiary.
	 */
	public boolean undoEntryAction()
	{
		EntryAction top;
		Entry temporaryEntry;
		Entry originalEntry;

		if (entryActionStack == null)
		{
			return false;
		}
		top = entryActionStack.pop();
		temporaryEntry = top.getEntry();
		originalEntry = top.getOriginalEntry();

		if (top.getType() == EntryAction.Type.add)
		{
			Time searchStartTime = temporaryEntry.getStartTime();
			int numberOfSlots = temporaryEntry.getNumberOfSlots();
			String date = temporaryEntry.getDate();

			for (int i = 0; i < meetingDiary.size(); i++)
			{
				if (searchStartTime == meetingDiary.get(i).getStartTime()
				        && numberOfSlots == meetingDiary.get(i).getNumberOfSlots()
				        && date.equals(meetingDiary.get(i).getDate()))
				{
					meetingDiary.remove(i);
				}
			}
		} else if (top.getType() == EntryAction.Type.delete)
		{
			meetingDiary.add(temporaryEntry);

		} else if (top.getType() == EntryAction.Type.edit)
		{
			Time searchStartTime = temporaryEntry.getStartTime();
			int numberOfSlots = temporaryEntry.getNumberOfSlots();
			String date = temporaryEntry.getDate();

			for (int i = 0; i < meetingDiary.size(); i++)
			{
				if (searchStartTime == meetingDiary.get(i).getStartTime()
				        && numberOfSlots == meetingDiary.get(i).getNumberOfSlots()
				        && date.equals(meetingDiary.get(i).getDate()))
				{
					meetingDiary.remove(i);
					meetingDiary.add(originalEntry);
				}
			}
		} else
		{
			return false;
		}
		return true;
	}
}
