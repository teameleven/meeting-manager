package manager;

import java.awt.EventQueue;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import ui.GUI;

public class Main
{
	public static Main main = new Main();

	public static void main(String[] args) throws Exception

	{
		main.startGUI();

		main.company = new Company();
	}

	private Company company;

	private Employee currentlyLoggedIn;

	private Week week;

	public Main()
	{
		currentlyLoggedIn = null;
	}

	public Company getCompany()
	{
		return company;
	}

	public Week getWeek()
	{
		return week;
	}

	public Week getWeek(int ID, String startDate, String endDate)
	{

		return week;
	}

	public Week getWeek(String startDate, String endDate)
	{
		return week;
	}

	public Employee loggedIn()
	{
		return currentlyLoggedIn;
	}

	public boolean login(int ID)
	{
		currentlyLoggedIn = main.company.findEmployee(ID);

		if (currentlyLoggedIn != null)
		{
			return true;
		}

		currentlyLoggedIn = null;
		return false;
	}

	public void logOut()
	{
		currentlyLoggedIn = null;
	}

	public void printDiaryCount()
	{
		System.out.println(getCompany().meetingDiary.size());
	}

	void startGUI()
	{
		try
		{
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException e1)
		{
			e1.printStackTrace();
		} catch (InstantiationException e1)
		{
			e1.printStackTrace();
		} catch (IllegalAccessException e1)
		{
			e1.printStackTrace();
		} catch (UnsupportedLookAndFeelException e1)
		{
			e1.printStackTrace();
		}

		EventQueue.invokeLater(new Runnable()
		{
			@Override
			public void run()
			{
				try
				{
					GUI.showMenu();
				} catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}
}
