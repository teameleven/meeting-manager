package manager;

public class Time
{
	private int hour;
	private int minute;

	public Time(String time)
	{
		hour = Integer.parseInt("" + time.charAt(0) + time.charAt(1));
		minute = Integer.parseInt("" + time.charAt(2) + time.charAt(3));
	}

	public int getHour()
	{
		return hour;
	}

	public int getMinute()
	{
		return minute;
	}

	@Override
	public String toString()
	{
		String hourString;
		String minuteString;
		if (hour < 10)
		{
			hourString = "0" + hour;
		} else
		{
			hourString = "" + hour;
		}

		if (minute < 10)
		{
			minuteString = "0" + minute;
		} else
		{
			minuteString = "" + minute;
		}

		return hourString + minuteString;
	}
}
