package manager;

/**
 * This class is used to store the data of each action the user does when
 * regarding entries. This will be for when an entry is added, deleted or
 * edited. The class primarily contains accessor and mutator methods. The class
 * stores an enum called Type that can either by add,delete or edit. This is so
 * the action can easily be identified. The class stores an Entry called entry,
 * simply storing the data of the Entry that has been manipulated The class
 * stores another Entry called orginalEntry, this is solely for when the Entry
 * is of type edit, this is so the unedited version of the entry can be restored
 * when the action is undone.
 */
public class EntryAction
{
	public enum Type
	{
		add, delete, edit
	};

	private Entry entry;
	private Entry originalEntry;
	private Type type;

	public EntryAction()
	{
		entry = new Entry();
		originalEntry = null;
	}

	public EntryAction(Entry entry)
	{
		this.entry = entry;
		this.originalEntry = null;
	}

	public EntryAction(Entry entry, Entry originalEntry)
	{
		this.entry = entry;
		this.originalEntry = originalEntry;
	}

	public Entry getEntry()
	{
		return entry;
	}

	public Entry getOriginalEntry()
	{
		return originalEntry;
	}

	public Type getType()
	{
		return type;
	}

	public void setTypeToAdd()
	{
		type = Type.add;
	}

	public void setTypeToDelete()
	{
		type = Type.delete;
	}

	public void setTypeToEdit()
	{
		type = Type.edit;
	}

}
