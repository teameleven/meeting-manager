package manager;

/**
 * This class handles all of the data needed for each meeting. Each meeting
 * needs a starting time, the number of 15 minute slots it will take up, the
 * type/description of the meeting the date of the meeting and an array of IDs
 * of the employees that are attending this meeting. This class is primarily
 * full of accessor and mutator methods.
 *
 */
public class Entry
{
	private String date;
	private int[] employeeIDs;
	private int numberOfSlots;
	private Time startTime;
	private String type;

	public Entry()
	{
		startTime = null;
		numberOfSlots = 0;
		type = null;
		date = null;
	}

	public Entry(Time startTime, int numberOfSlots, String type, String date, int[] employeeIDs)
	{
		this.startTime = startTime;
		this.numberOfSlots = numberOfSlots;
		this.type = type;
		this.date = date;
		this.employeeIDs = employeeIDs;
	}

	public String getDate()
	{
		return date;
	}

	public int[] getEmployeeIDs()
	{
		return employeeIDs;
	}

	public int getNumberOfSlots()
	{
		return numberOfSlots;
	}

	public Time getStartTime()
	{
		return startTime;
	}

	public String getType()
	{
		return type;
	}

	public void setDate(String date)
	{
		this.date = date;
	}

	public void setEmployeeIDs(int[] employeeIDs)
	{
		this.employeeIDs = employeeIDs;
	}

	public void setNumberOfSlots(int numberOfSlots)
	{
		this.numberOfSlots = numberOfSlots;
	}

	public void setStartTime(Time startTime)
	{
		this.startTime = startTime;
	}

	public void setType(String type)
	{
		this.type = type;
	}

	@Override
	public String toString()
	{
		return type + " " + date + " " + startTime;
	}
}
