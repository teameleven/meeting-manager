package manager;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.nio.file.Paths;
import java.util.Scanner;

public class SaveManager
{
	private final static String PATH = "save-file.txt";

	public static void openData()
	{
		try
		{
			Scanner in = new Scanner(Paths.get(PATH), "UTF-8");

			while (in.hasNextLine())
			{

				String startTime = in.nextLine();
				int numberOfSlots = Integer.parseInt(in.nextLine());
				String type = in.nextLine();
				String date = in.nextLine();
				int[] employeeIDs = new int[Integer.parseInt(in.nextLine())];
				String[] employeeID = in.nextLine().split(",");
				int counter = 0;
				for (String s : employeeID)
				{
					employeeIDs[counter++] = Integer.parseInt(s);
				}
				Main.main.getCompany().addEntry(new Time(startTime), numberOfSlots, type, date, employeeIDs);
			}

			in.close();
		} catch (Exception e)
		{
			System.out.println("An error has occured");
		}
	}

	public static void saveData()
	{
		StringBuilder sb = new StringBuilder();
		for (Entry eachEntry : Main.main.getCompany().meetingDiary)
		{
			sb.append(eachEntry.getStartTime().toString());
			sb.append(System.getProperty("line.separator"));
			sb.append(eachEntry.getNumberOfSlots());
			sb.append(System.getProperty("line.separator"));
			sb.append(eachEntry.getType());
			sb.append(System.getProperty("line.separator"));
			sb.append(eachEntry.getDate());
			sb.append(System.getProperty("line.separator"));
			sb.append(eachEntry.getEmployeeIDs().length);
			sb.append(System.getProperty("line.separator"));

			for (Integer eachEmployeeID : eachEntry.getEmployeeIDs())
			{
				sb.append(eachEmployeeID.toString() + ",");
			}

			sb.append(System.getProperty("line.separator"));
		}
		String savedData = sb.toString();
		try
		{
			FileWriter fileWriter = new FileWriter(PATH);
			PrintWriter printWriter = new PrintWriter(fileWriter);
			printWriter.print(savedData);
			printWriter.close();
		} catch (Exception e)
		{
			System.out.println("An error has occured");
		}
	}
}
