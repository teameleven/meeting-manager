/**
 * 
 */
package manager;

import java.util.ArrayList;

/**
 * This class stores all of the data required for each week within the
 * meetingDiary. This class basically will be used to concisely receive all of
 * the entries that will take place for that date. This class stores ArrayLists
 * of Entries for each weekday. This class stores a String variable called
 * startDate and one called endDate, to identify the start and end of the week.
 * This class primarily contains accessor and mutator methods.
 */
public class Week
{
	private String endDate;
	private ArrayList<Entry> friday;
	private ArrayList<Entry> monday;
	private String startDate;
	private ArrayList<Entry> thursday;
	private ArrayList<Entry> tuesday;
	private ArrayList<Entry> wednesday;

	/**
	 * 
	 */
	public Week()
	{
		monday = new ArrayList<Entry>();
		tuesday = new ArrayList<Entry>();
		wednesday = new ArrayList<Entry>();
		thursday = new ArrayList<Entry>();
		friday = new ArrayList<Entry>();
		startDate = null;
		endDate = null;
	}

	public Week(ArrayList<Entry> monday, ArrayList<Entry> tuesday, ArrayList<Entry> wednesday,
	        ArrayList<Entry> thursday, ArrayList<Entry> friday, String startDate, String endDate)
	{
		this.monday = monday;
		this.tuesday = tuesday;
		this.wednesday = wednesday;
		this.thursday = thursday;
		this.friday = friday;
		this.startDate = startDate;
		this.endDate = endDate;
	}

	public String getEndDate()
	{
		return endDate;
	}

	public ArrayList<Entry> getFriday()
	{
		return friday;
	}

	public ArrayList<Entry> getMonday()
	{
		return monday;
	}

	public String getStartDate()
	{
		return startDate;
	}

	public ArrayList<Entry> getThursday()
	{
		return thursday;
	}

	public ArrayList<Entry> getTuesday()
	{
		return tuesday;
	}

	public ArrayList<Entry> getWednesday()
	{
		return wednesday;
	}

	public void setEndDate(String endDate)
	{
		this.endDate = endDate;
	}

	public void setFriday(ArrayList<Entry> friday2)
	{
		for (int i = 0; i < friday2.size(); i++)
		{
			friday.add(friday2.get(i));
		}
	}

	public void setMonday(ArrayList<Entry> monday2)
	{
		for (int i = 0; i < monday2.size(); i++)
		{
			monday.add(monday2.get(i));
		}
	}

	public void setStartDate(String startDate)
	{
		this.startDate = startDate;
	}

	public void setThursday(ArrayList<Entry> thursday2)
	{
		for (int i = 0; i < thursday2.size(); i++)
		{
			thursday.add(thursday2.get(i));
		}
	}

	public void setTuesday(ArrayList<Entry> tuesday2)
	{
		for (int i = 0; i < tuesday2.size(); i++)
		{
			tuesday.add(tuesday2.get(i));
		}
	}

	public void setWednesday(ArrayList<Entry> wednesday2)
	{
		for (int i = 0; i < wednesday2.size(); i++)
		{
			wednesday.add(wednesday2.get(i));
		}
	}

}
