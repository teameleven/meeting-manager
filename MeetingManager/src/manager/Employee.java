package manager;

/**
 * This class stores all of the data required for each Employee within the
 * Company. This class stores a String variable called name, obviously for the
 * name of the Employee. This class stores an int variable called ID, storing
 * the ID of the Employee.
 *
 */
public class Employee
{
	private int ID;
	private String name;

	public Employee()
	{
		name = null;
		ID = 0;
	}

	public Employee(String name, int ID)
	{
		this.name = name;
		this.ID = ID;
	}

	public int getID()
	{
		return ID;
	}

	public String getName()
	{
		return name;
	}

	public void setID(int ID)
	{
		this.ID = ID;
	}

	public void setName(String name)
	{
		this.name = name;
	}

}
